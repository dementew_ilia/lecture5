package ru.sbrf.edu;


import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class PersonTest {

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionAddNewPerson(){
        Person testPersonNullName = new Person(null, "Moscow", 30);
        Person testPersonNullCity = new Person("Andrey", null, 20);
        Person testPersonNullCityAndNullName = new Person(null, null, 25);
    }

    /** Проверка корректности работы сортировки сначала по полю
     * city, затем по полю name*/
    @Test
    public void testCompareTo(){
        List<Person> testList = new ArrayList<>();

        String [] userName = {"Andrey", "Boris", "Ivan"};
        String [] city = {"Astrakhan", "Moscow", "Saint Petersburg"};
        Person testPerson1 = new Person(userName[0], city[1], 30);
        Person testPerson2 = new Person(userName[1], city[0], 30);
        Person testPerson3 = new Person(userName[1], city[2], 30);


        testList.add(testPerson1);
        testList.add(testPerson2);
        testList.add(testPerson3);

        testList.sort(Person::compareTo);

        Assertions.assertEquals(testList.get(0).getCity(), city[0]);
        Assertions.assertEquals(testList.get(0).getName(), userName[1]);

        Assertions.assertEquals(testList.get(1).getCity(), city[1]);
        Assertions.assertEquals(testList.get(1).getName(), userName[0]);

        Assertions.assertEquals(testList.get(2).getCity(), city[2]);
        Assertions.assertEquals(testList.get(2).getName(), userName[1]);
    }
}