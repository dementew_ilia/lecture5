package ru.sbrf.edu;

import java.util.Comparator;

/**
 * Класс который реализует логику по изменению порядка чисел (Сначала четные числа, затем нечетные)
 * */
public class CustomDigitComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer lhs, Integer rhs) {
        if(lhs == null || rhs == null){
            throw new NullPointerException("сравнение происходит только по числам, отличным от null");
        }
        return Integer.compare(lhs % 2, rhs % 2);
    }
}
