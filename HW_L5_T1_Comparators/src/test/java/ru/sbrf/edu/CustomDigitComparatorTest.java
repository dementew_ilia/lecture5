package ru.sbrf.edu;

import org.junit.Test;
import org.junit.jupiter.api.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class CustomDigitComparatorTest {
    @Test
    public void compareTest(){
        Integer [] list = {1, 2, 3, 4, 5};
        List <Integer> testList = Arrays.asList(list);

        Integer [] listAfterSort = {2, 4, 1, 3, 5};

        Comparator<Integer> digitComparator = new CustomDigitComparator();
        testList.sort(digitComparator);

        for(int i =0; i < testList.size(); i++){
            Assertions.assertEquals(testList.get(i), listAfterSort[i]);
        }
    }
}
