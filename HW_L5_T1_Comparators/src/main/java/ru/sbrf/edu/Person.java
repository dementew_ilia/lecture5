package ru.sbrf.edu;

public class Person implements Comparable<Person>{
    private String name;
    private String city;
    private int age;

    public Person(String name, String city, int age) {
        if(city == null || name == null){
            throw new IllegalArgumentException("name и city не должны быть null");
        }

        this.name = name;
        this.city = city;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    /**
     * Сравнение по полю city, а затем по полю name
     */
    @Override
    public int compareTo(Person other) {
        int compareByCity = this.city.compareTo(other.city);
        int compareByName = this.name.compareTo(other.name);
        if(compareByCity == 0){
            return compareByName;
        }
        return compareByCity;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}
